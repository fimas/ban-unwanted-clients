#!/usr/bin/python3
#
# A simple program that polls the access log of an Apache web server
# that checks for strange requests and banns the client if they make
# to many strange requests
#

hostname="localhost"
username=""
password=""
database=""

import os
import sys
import time
import subprocess
import shlex
import csv
import mysql.connector

def checkIfIPInList(db, ip):
    crs = db.cursor()
    crs.execute("SELECT ip FROM banlist WHERE ip=%s", (ip,))
    result = crs.fetchone()

    if result is None:
        return False

    return True

def Banfun(hostname, username, password, database):
    mydb = mysql.connector.connect(
                host=hostname,
                user=username,
                passwd=password,
                database=database)

    mycursor = mydb.cursor()


    ufw = subprocess.check_output(
            shlex.split(
                "which ufw"))

    iptables = subprocess.check_output(
            shlex.split(
                "which iptables"))

    with open("/var/log/apache2/access.log", 'r') as AccessLog:
        reader = csv.reader(AccessLog, delimiter=' ', quotechar='"')

        offenders = {}

        for row in reader:
            if row[6] == "400":
                if row[0] in offenders:
                    offenders[row[0]] += 1
                else:
                    offenders[row[0]] = 1

    for ip in offenders:
        if offenders[ip] > 2:
           if checkIfIPInList(mydb, ip):
               continue
           else:
               print("Banning {}".format(ip))
               mycursor.execute("INSERT INTO banlist(ip) VALUES(%s)", (ip,))
               mydb.commit()
               with open(os.devnull, 'w') as DevNull:
                   subprocess.call(
                       shlex.split(
                           "{0} insert 1 deny from {1} to any".format(
                           ufw.decode(), ip)),
                        stdout=DevNull)

class Watcher(object):
    running = True
    refresh_delay_secs = 1

    def __init__(self, watch_file, call_func_on_change=None, *args, **kwargs):
        self._cached_stamp = 0
        self.filename = watch_file
        self.call_func_on_change = call_func_on_change
        self.args = args
        self.kwargs = kwargs

    def look(self):
        stamp = os.stat(self.filename).st_mtime
        if stamp != self._cached_stamp:
            self._cached_stamp = stamp
            if self.call_func_on_change is not None:
                self.call_func_on_change(*self.args, **self.kwargs)

    def watch(self):
        while self.running:
            try:
                time.sleep(self.refresh_delay_secs)
                self.look()
            except KeyboardInterrupt:
                print('\nDone')
                break
            except FileNotFoundError:
                print('File not found')
                self.running = False
            except:
                print('Unhandled error: %s' % sys.exc_info()[0])

watcher = Watcher('/var/log/apache2/access.log', Banfun, hostname, username, password, database)
watcher.watch()