#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <sys/stat.h>
#include <thread>
#include <chrono>
#include <functional>
#include <boost/tokenizer.hpp>
#include "mysql_connection.h"
#include "mysql_driver.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

using namespace std;
using namespace boost;
using namespace sql;

typedef vector<vector<string>> StringVector;
typedef escaped_list_separator<char> t_els;
typedef tokenizer<t_els> t_tokens;
typedef map<string, int> IPScore;

void readFile(string fn, stringstream * ss) {
  ifstream f(fn, ios::in);

  if (!f.good()) {
    cerr << "Could not read file!" << endl;
    return;
  }

  *ss << f.rdbuf();
  f.close();
}

StringVector fetchLines(stringstream * ss) {
  StringVector lines;
  string file = ss->str();
  t_els line_els(string(""), string("\n"), string("\""));
  t_els part_els(string(""), string(" "), string("\""));
  t_tokens line_tok(file, line_els);

  for (auto line : line_tok) {
    t_tokens part_tok(line, part_els);
    vector<string> parts;

    for (auto part : part_tok) {
      parts.push_back(part);
    }

    lines.push_back(parts);
  }

  return lines;
}

IPScore scoreHosts(StringVector hosts) {
  IPScore list;

  for (auto host : hosts) {
    if (host.size() < 8) {
      continue;
    }

    string ip = host[0];
    string code = host[7];

    if(host[6] == "400" || host[7] == "400") {
      if(list.find(ip) != list.end()) {
        list[ip] += 1;
      }
      else {
        list[ip] = 1;
      }
    }

  }

  return list;
}
bool isIPInDB(string ip, Connection * con) {
  PreparedStatement * pstmt;
  ResultSet * res;

  pstmt = con->prepareStatement(
      "SELECT COUNT(*) as count FROM banlist WHERE ip=?");
  pstmt->setString(1, ip);
  pstmt->execute();

  res = pstmt->executeQuery();

  bool exists = false;

  while (res->next()) {
    if (res->getInt("count") > 0) {
      exists = true;
    }
  }

  delete pstmt;
  delete res;

  return exists;
}

void insertIPIntoDB(string ip, Connection * con) {
  PreparedStatement * pstmt;
  pstmt = con->prepareStatement("INSERT INTO banlist(ip) VALUES(?)");
  pstmt->setString(1, ip);
  pstmt->execute();

  delete pstmt;
}

int insertIPIntoFirewall(string ip) {
  string cmd = "/usr/sbin/ufw insert 1 deny from " + ip + " to any > /dev/null 2>&1";
  return system(cmd.c_str());
}

void banIp(IPScore hosts, Connection * con) {
  for (auto baddie : hosts) {
    if (baddie.second > 2) {
      if(isIPInDB(baddie.first, con)) {
        continue;
      }

      cout << "Banning " << baddie.first << endl;
      insertIPIntoDB(baddie.first, con);
      insertIPIntoFirewall(baddie.first);
    }
  }
}

void mainLoop(string filename, function<void(string)> callback) {
  time_t lastChange = 0;
  struct stat st;
  bool running = true;

  while (running) {
    this_thread::sleep_for(chrono::milliseconds(1000));

    if (stat(filename.c_str(), &st) != 0) {
      cerr << "Could not stat file!" << endl;
      running = false;
    }
    else {
      if (lastChange != st.st_mtime) {
        callback(filename);
      }
    }
  }
}

int main() {
  string filename = "/var/log/apache2/access.log";
  Driver * driver = get_driver_instance();
  Connection * con = driver->connect(
      "localhost",
      "<username>",
      "<password>");
  con->setSchema("<database>");

  mainLoop(filename, [&] (string filename) {
        stringstream fileContents;

        readFile(filename, &fileContents);
        banIp(scoreHosts(fetchLines(&fileContents)), con);
      });

  return 0;
}