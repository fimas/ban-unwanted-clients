# Ban unwanted clients

A quick and dirty python script that looks up bad HTTP requests (error 400) and banns them if they make repeated bad requests.

If you have problem with attackers filling your Apache 2 access.log with useless information like this 

```
xxx.xxx.xxx.xxx - - [20/Feb/2019:06:03:20 +0000] "@\x88;\xc6\xa5)\xc9\x06\xc6\xd7\n" 400 0 "-" "-"
```

Then this script will help you.

# Using

The script uses polling to check if the access log has been updated. This means you can just start the script and leave it. Using screen or tmux is recommended.

Before you start the script you need to setup a database and a table according to the banlist.sql file.

# Software requirements

This script is written and tested using Python 3.6.7 on Ubuntu 18.04.2 LTS.

You also need the pip library mysql-connector installed. Just run

```
$ pip install mysql-connector
```

in your terminal to install it.

Currently the script only works with the Iptables frontend called UFW, but support for more implementations may be added in the future.